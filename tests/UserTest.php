<?php

namespace SpotifyClient\Tests;

use AutoMapperPlus\Exception\UnregisteredMappingException;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use SpotifyClient\Model\User\Me;
use SpotifyClient\SpotifyClient;
use SpotifyClient\SpotifyClientConfiguration;

class UserTest extends TestCase
{
    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     * @dataProvider provideToken
     */
    public function testGetMeRequest(string $token): void
    {
        $spotifyClient = new SpotifyClient(new SpotifyClientConfiguration('', '', 3));
        $me            = $spotifyClient->scopeUser()->getMe($token);
        $this->assertSame(get_class($me), Me::class);
    }

    public static function provideToken(): array
    {
        return [
            ['BQCJ97AaVbTl2x7v5C1FpOJkP8YSJ7mwvKhMgNL4pWnkOyEfOO6_a5SkSGASk54FJi5kh0sE5MzNhoaAwFRkXB80X5yA4_rKu72vfSCcd_SUXG3yRCDj3Wy4ZkpCUz1_7h036WZdMHXDPvXFCHaB3jqT7K7juzyfiTG5TvBBR2M7fns57rvgV7tEfBxydao']
        ];
    }
}
