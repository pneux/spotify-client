<?php

namespace SpotifyClient\Tests;

use AutoMapperPlus\Exception\UnregisteredMappingException;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use SpotifyClient\Model\Auth\TokenResponse;
use SpotifyClient\SpotifyClient;
use SpotifyClient\SpotifyClientConfiguration;

class AuthTest extends TestCase
{
    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function testSendsRetrieveTokenRequest(): void
    {
        // get inputs
        $clientId     = '';
        $clientSecret = '';
        $code         = '';
        $redirectUri  = '';

        // send request
        $spotifyClient = new SpotifyClient(new SpotifyClientConfiguration($clientId, $clientSecret, 3));
        $token = $spotifyClient->scopeAuth()->retrieveToken($code, $redirectUri, 'authorization_code');

        $this->assertSame(get_class($token), TokenResponse::class);
    }
}
