<?php

namespace SpotifyClient;

class SpotifyClientConfiguration
{
    private string $clientId;
    private string $clientSecret;
    private float  $timeout;

    public function __construct(string $clientId, string $clientSecret, float $timeout)
    {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
        $this->timeout      = $timeout;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @return float
     */
    public function getTimeout(): float
    {
        return $this->timeout;
    }
}