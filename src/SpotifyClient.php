<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient;

use GuzzleHttp\Client;
use SpotifyClient\Request\ResponseMapper;
use SpotifyClient\Scopes\AuthScope;
use SpotifyClient\Scopes\PlaylistScope;
use SpotifyClient\Scopes\UserScope;

final class SpotifyClient
{
    private Client                     $client;
    private Client                     $authClient;
    private SpotifyClientConfiguration $configuration;
    private ResponseMapper             $mapper;

    public function __construct(SpotifyClientConfiguration $configuration)
    {
        $this->client        = new Client([
            'base_uri' => 'https://api.spotify.com',
            'timeout'  => $configuration->getTimeout()
        ]);
        $this->authClient    = new Client([
            'base_uri' => 'https://accounts.spotify.com',
            'timeout'  => $configuration->getTimeout()
        ]);
        $this->configuration = $configuration;
        $this->mapper        = new ResponseMapper();
    }

    public function scopeAuth(): AuthScope
    {
        return new AuthScope($this->authClient, $this->mapper, $this->configuration);
    }

    public function scopeUser(): UserScope
    {
        return new UserScope($this->client, $this->mapper);
    }

    public function scopePlaylist(): PlaylistScope
    {
        return new PlaylistScope($this->client, $this->mapper);
    }
}
