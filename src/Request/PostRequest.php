<?php

namespace SpotifyClient\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

abstract class PostRequest extends Request
{
    /**
     * @throws GuzzleException
     */
    public function send(Client $client, string $token): ResponseInterface
    {
        return $client->post($this->getPath(), [
            'query' => $this->getQuery(),
            'headers' => [
                'Authorization' => sprintf('Bearer %s', $token)
            ]
        ]);
    }

    protected abstract function getBody();
}
