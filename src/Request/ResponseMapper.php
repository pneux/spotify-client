<?php

namespace SpotifyClient\Request;

use Psr\Http\Message\ResponseInterface;
use SpotifyClient\Model\Album\Album;
use SpotifyClient\Model\Album\AlbumGroup;
use SpotifyClient\Model\Album\AlbumReleaseDatePrecision;
use SpotifyClient\Model\Album\AlbumType;
use SpotifyClient\Model\Artist\Artist;
use SpotifyClient\Model\Artist\SimplifiedArtist;
use SpotifyClient\Model\Auth\TokenResponse;
use SpotifyClient\Model\ExternalIds;
use SpotifyClient\Model\ExternalUrls;
use SpotifyClient\Model\Followers\Followers;
use SpotifyClient\Model\Image;
use SpotifyClient\Model\ObjectType;
use SpotifyClient\Model\Playlist\Playlist;
use SpotifyClient\Model\Playlist\PlaylistList;
use SpotifyClient\Model\Playlist\PlaylistTrack;
use SpotifyClient\Model\Playlist\PlaylistTrackList;
use SpotifyClient\Model\Playlist\SimplifiedPlaylist;
use SpotifyClient\Model\Track\Track;
use SpotifyClient\Model\User\ExplicitContentSettings;
use SpotifyClient\Model\User\Me;
use SpotifyClient\Model\User\MyTopItemsList;
use SpotifyClient\Model\User\User;
use SpotifyClient\Model\User\UserBase;
use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use AutoMapperPlus\MappingOperation\Operation;
use DateTime;

class ResponseMapper
{
    private AutoMapper $mapper;

    public function __construct()
    {
        $config = new AutoMapperConfig();

        // USER
        $baseUserMapping = $config->registerMapping('array', UserBase::class)
            ->forMember('externalUrls', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['external_urls'], ExternalUrls::class))
            ->forMember('followers', Operation::mapTo(Followers::class, true))
            ->forMember('type', fn ($obj) => ObjectType::tryFrom($obj['type']));

        $userMapping = $config->registerMapping('array', User::class)->copyFromMapping($baseUserMapping)
            ->forMember('displayName', Operation::fromProperty('display_name'))
            ->forMember('images', Operation::mapCollectionTo(Image::class));

        $config->registerMapping('array', Me::class)->copyFromMapping($userMapping)
            ->forMember('country', fn (array $obj) => $obj['country'] ?? null)
            ->forMember('email', fn (array $obj) => $obj['email'] ?? null)
            ->forMember('explicitContent', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['explicit_content'] ?? null, ExplicitContentSettings::class))
            ->forMember('product', fn (array $obj) => $obj['product'] ?? null);

        $config->registerMapping('array', ExplicitContentSettings::class)
            ->forMember('filterEnabled', Operation::fromProperty('filter_enabled'))
            ->forMember('filterLocked', Operation::fromProperty('filter_locked'));

        $config->registerMapping('array', MyTopItemsList::class)
            ->forMember('items', function (array $obj, AutoMapper $mapper) {
                $item = $obj['items'][0] ?? [];
                if ($item['type'] === ObjectType::Artist->value) {
                    return $mapper->mapMultiple($obj['items'], Artist::class);
                }
                if ($item['type'] === ObjectType::Track->value) {
                    return $mapper->mapMultiple($obj['items'], Track::class);
                }
                return [];
            });

        // PLAYLIST
        $simplifiedPlaylistMapping = $config->registerMapping('array', SimplifiedPlaylist::class)
            ->forMember('externalUrls', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['external_urls'], ExternalUrls::class))
            ->forMember('images', Operation::mapCollectionTo(Image::class))
            ->forMember('owner', Operation::mapTo(User::class, true))
            ->forMember('snapshotId', Operation::fromProperty('snapshot_id'))
            ->forMember('tracks', Operation::mapTo(PlaylistTrackList::class, true))
            ->forMember('type', fn ($obj) => ObjectType::tryFrom($obj['type']));

        $config->registerMapping('array', Playlist::class)->copyFromMapping($simplifiedPlaylistMapping)
            ->forMember('followers', Operation::mapTo(Followers::class, true));

        $config->registerMapping('array', PlaylistTrackList::class)
            ->forMember('items', Operation::mapCollectionTo(PlaylistTrack::class));

        $config->registerMapping('array', PlaylistList::class)
            ->forMember('items', Operation::mapCollectionTo(SimplifiedPlaylist::class));

        $config->registerMapping('array', PlaylistTrack::class)
            ->forMember('addedAt', fn (array $obj) => new DateTime($obj['added_at']))
            ->forMember('addedBy', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['added_by'], UserBase::class))
            ->forMember('isLocal', Operation::fromProperty('is_local'))
            ->forMember('track', function (array $obj, AutoMapper $mapper) {
                if ($obj['track']['type'] === 'track') {
                    return $mapper->map($obj['track'], Track::class);
                }
                return null;
            });

        // TRACK
        $config->registerMapping('array', Track::class)
            ->forMember('album', Operation::mapTo(Album::class, true))
            ->forMember('artists', Operation::mapCollectionTo(Artist::class))
            ->forMember('availableMarkets', Operation::fromProperty('available_markets'))
            ->forMember('discNumber', Operation::fromProperty('disc_number'))
            ->forMember('durationMs', Operation::fromProperty('duration_ms'))
            ->forMember('externalIds', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['external_ids'], ExternalIds::class))
            ->forMember('externalUrls', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['external_urls'], ExternalUrls::class))
            ->forMember('isLocal', Operation::fromProperty('is_local'))
            ->forMember('previewUrl', Operation::fromProperty('preview_url'))
            ->forMember('trackNumber', Operation::fromProperty('track_number'))
            ->forMember('type', fn ($obj) => ObjectType::tryFrom($obj['type']));

        // ALBUM
        $config->registerMapping('array', Album::class)
            ->forMember('albumGroup', fn ($obj) => array_key_exists('album_group', $obj) ? AlbumGroup::tryFrom(strtolower($obj['album_group'])) : null)
            ->forMember('albumType', fn ($obj) => AlbumType::tryFrom(strtolower($obj['album_type'])))
            ->forMember('type', fn ($obj) => ObjectType::tryFrom($obj['type']))
            ->forMember('artists', Operation::mapCollectionTo(SimplifiedArtist::class))
            ->forMember('availableMarkets', Operation::fromProperty('available_markets'))
            ->forMember('externalUrls', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['external_urls'], ExternalUrls::class))
            ->forMember('images', Operation::mapCollectionTo(Image::class))
            ->forMember('releaseDate', fn (array $obj) => new DateTime($obj['release_date']))
            ->forMember('releaseDatePrecision', fn ($obj) => AlbumReleaseDatePrecision::tryFrom($obj['release_date_precision']))
            ->forMember('totalTracks', Operation::fromProperty('total_tracks'));

        // ARTISTS
        $simplifiedArtistMapping = $config->registerMapping('array', SimplifiedArtist::class)
            ->forMember('externalUrls', fn (array $obj, AutoMapper $mapper) => $mapper->map($obj['external_urls'], ExternalUrls::class))
            ->forMember('type', fn ($obj) => ObjectType::tryFrom($obj['type']));

        $config->registerMapping('array', Artist::class)->copyFromMapping($simplifiedArtistMapping)
            ->forMember('followers', Operation::mapTo(Followers::class, true))
            ->forMember('images', Operation::mapCollectionTo(Image::class));

        // AUTH
        $config->registerMapping('array', TokenResponse::class)
            ->forMember('accessToken', Operation::fromProperty('access_token'))
            ->forMember('tokenType', Operation::fromProperty('token_type'))
            ->forMember('scope', fn (array $obj) => explode(' ', $obj['scope']))
            ->forMember('expiresIn', Operation::fromProperty('expires_in'))
            ->forMember('refreshToken', Operation::fromProperty('refresh_token'));

        // GENERAL
        $config->registerMapping('array', ExternalIds::class)
            ->forMember('isrc', fn (array $obj) => $obj['isrc'] ?? null)
            ->forMember('ean', fn (array $obj) => $obj['ean'] ?? null)
            ->forMember('upc', fn (array $obj) => $obj['upc'] ?? null);

        $config->registerMapping('array', ExternalUrls::class);

        $config->registerMapping('array', Followers::class);

        $config->registerMapping('array', Image::class);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @template T
     * @param ResponseInterface $response
     * @param class-string<T> $destinationClass
     * @return T
     * @throws UnregisteredMappingException
     */
    public function map(ResponseInterface $response, string $destinationClass)
    {
        return $this->mapper->map(json_decode($response->getBody(), true), $destinationClass);
    }
}
