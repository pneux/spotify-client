<?php

namespace SpotifyClient\Request;

use GuzzleHttp\Client;

abstract class Request
{
    public abstract function send(Client $client, string $token);

    protected abstract function getPath(): string;
}
