<?php

namespace SpotifyClient\Request\Api\Playlists;

use SpotifyClient\Request\GetRequest;

class GetPlaylistRequest extends GetRequest
{

    private string $playlistId;

    public function __construct(string $playlistId)
    {
        $this->playlistId = $playlistId;
    }

    protected function getPath(): string
    {
        return sprintf('v1/playlists/%s', $this->playlistId);
    }
}
