<?php

namespace SpotifyClient\Request\Api\Playlists;

use SpotifyClient\Request\GetRequest;

class GetMyPlaylistsRequest extends GetRequest
{

    protected function getPath(): string
    {
        return 'v1/me/playlists';
    }
}
