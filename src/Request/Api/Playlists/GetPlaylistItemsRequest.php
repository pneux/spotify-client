<?php

namespace SpotifyClient\Request\Api\Playlists;

use SpotifyClient\Request\GetRequest;

class GetPlaylistItemsRequest extends GetRequest
{

    private string $playlistId;
    private ?int   $limit;
    private ?int   $offset;

    public function __construct(string $playlistId, ?int $limit = null, ?int $offset = null)
    {
        $this->playlistId = $playlistId;
        $this->limit      = $limit;
        $this->offset     = $offset;
    }

    protected function getPath(): string
    {
        return sprintf('v1/playlists/%s/tracks', $this->playlistId);
    }

    protected function getQuery(): ?array
    {
        return [
            'limit'  => $this->limit,
            'offset' => $this->offset
        ];
    }
}
