<?php

namespace SpotifyClient\Request\Api\Auth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use SpotifyClient\Request\Request;

class RefreshTokenRequest extends Request
{
    private string $refreshToken;

    public function __construct(string $refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    protected function getPath(): string
    {
        return 'api/token';
    }

    /**
     * @throws GuzzleException
     */
    public function send(Client $client, string $token): ResponseInterface
    {
        return $client->post($this->getPath(), [
            'headers'     => [
                'Authorization' => sprintf('Basic %s', $token)
            ],
            'form_params' => [
                'grant_type'    => 'refresh_token',
                'refresh_token' => $this->refreshToken,
            ]
        ]);
    }
}
