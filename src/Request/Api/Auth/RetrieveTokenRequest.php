<?php

namespace SpotifyClient\Request\Api\Auth;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use SpotifyClient\Request\Request;

class RetrieveTokenRequest extends Request
{
    private string $code;
    private string $redirectUri;

    public function __construct(string $code, string $redirectUri)
    {
        $this->code        = $code;
        $this->redirectUri = $redirectUri;
    }

    protected function getPath(): string
    {
        return 'api/token';
    }

    /**
     * @throws GuzzleException
     */
    public function send(Client $client, string $token): ResponseInterface
    {
        return $client->post($this->getPath(), [
            'headers'     => [
                'Authorization' => sprintf('Basic %s', $token)
            ],
            'form_params' => [
                'grant_type'   => 'authorization_code',
                'code'         => $this->code,
                'redirect_uri' => $this->redirectUri,
            ]
        ]);
    }
}
