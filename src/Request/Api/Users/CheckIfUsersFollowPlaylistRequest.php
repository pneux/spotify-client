<?php

namespace SpotifyClient\Request\Api\Users;

use SpotifyClient\Request\GetRequest;

class CheckIfUsersFollowPlaylistRequest extends GetRequest
{
    private string $playlistId;

    /** @var string[] */
    private array  $userIds;

    public function __construct(string $playlistId, array $userIds)
    {
        $this->playlistId = $playlistId;
        $this->userIds = $userIds;
    }

    protected function getPath(): string
    {
        return sprintf('v1/playlists/%s/followers/contains', $this->playlistId);
    }

    protected function getQuery(): ?array
    {
        return [
            'ids' => implode(',', $this->userIds)
        ];
    }
}
