<?php

namespace SpotifyClient\Request\Api\Users;

enum MyTopItemType: string
{
    case Artists = 'artists';
    case Tracks = 'tracks';
}
