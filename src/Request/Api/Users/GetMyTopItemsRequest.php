<?php

namespace SpotifyClient\Request\Api\Users;

use SpotifyClient\Request\GetRequest;

class GetMyTopItemsRequest extends GetRequest
{

    private MyTopItemType       $type;
    private ?MyTopItemTimeRange $timeRange;
    private ?int                $limit;
    private ?int                $offset;

    public function __construct(
        MyTopItemType       $type,
        ?MyTopItemTimeRange $timeRange = null,
        ?int                $limit = null,
        ?int                $offset = null
    )
    {
        $this->type      = $type;
        $this->timeRange = $timeRange;
        $this->limit     = $limit;
        $this->offset    = $offset;
    }

    protected function getPath(): string
    {
        return sprintf('v1/me/top/%s', $this->type->value);
    }

    protected function getQuery(): ?array
    {
        return [
            'time_range' => $this->timeRange?->value,
            'limit'      => $this->limit,
            'offset'     => $this->offset
        ];
    }
}
