<?php

namespace SpotifyClient\Request\Api\Users;

enum MyTopItemTimeRange: string
{
    case Long = 'long_term';
    case Medium = 'medium_term';
    case Short = 'short_term';
}
