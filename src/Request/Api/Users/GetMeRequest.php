<?php

namespace SpotifyClient\Request\Api\Users;

use SpotifyClient\Request\GetRequest;

class GetMeRequest extends GetRequest
{
    protected function getPath(): string
    {
        return 'v1/me';
    }
}
