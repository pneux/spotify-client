<?php

namespace SpotifyClient\Request\Api\Users;

use SpotifyClient\Request\GetRequest;

class GetUserRequest extends GetRequest
{
    private string $userId;

    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    protected function getPath(): string
    {
        return sprintf('v1/users/%s', $this->userId);
    }
}
