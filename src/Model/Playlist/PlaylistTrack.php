<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Playlist;

use SpotifyClient\Model\User\UserBase;
use DateTimeInterface;

class PlaylistTrack
{
    private ?DateTimeInterface $addedAt;
    private ?UserBase          $addedBy;
    private bool               $isLocal;
    private PlaylistTrackTrack $track;

    /**
     * @return DateTimeInterface|null
     */
    public function getAddedAt(): ?DateTimeInterface
    {
        return $this->addedAt;
    }

    /**
     * @return UserBase|null
     */
    public function getAddedBy(): ?UserBase
    {
        return $this->addedBy;
    }

    /**
     * @return bool
     */
    public function isLocal(): bool
    {
        return $this->isLocal;
    }

    /**
     * @return PlaylistTrackTrack
     */
    public function getTrack(): PlaylistTrackTrack
    {
        return $this->track;
    }
}
