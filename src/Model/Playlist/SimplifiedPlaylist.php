<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Playlist;

use SpotifyClient\Model\ExternalUrls;
use SpotifyClient\Model\Image;
use SpotifyClient\Model\ObjectType;
use SpotifyClient\Model\User\User;

class SimplifiedPlaylist
{
    protected bool         $collaborative;
    protected ?string      $description;
    protected ExternalUrls $externalUrls;

    protected string $href;
    protected string $id;

    /** @var Image[] */
    protected array $images;

    protected string            $name;
    protected User              $owner;
    protected bool              $public;
    protected string            $snapshotId;
    protected PlaylistTrackList $tracks;
    protected ObjectType        $type;
    protected string            $uri;

    /**
     * @return bool
     */
    public function isCollaborative(): bool
    {
        return $this->collaborative;
    }

    /**
     * @param bool $collaborative
     */
    public function setCollaborative(bool $collaborative): void
    {
        $this->collaborative = $collaborative;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return ExternalUrls
     */
    public function getExternalUrls(): ExternalUrls
    {
        return $this->externalUrls;
    }

    /**
     * @param ExternalUrls $externalUrls
     */
    public function setExternalUrls(ExternalUrls $externalUrls): void
    {
        $this->externalUrls = $externalUrls;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @param string $href
     */
    public function setHref(string $href): void
    {
        $this->href = $href;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages(array $images): void
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->public;
    }

    /**
     * @param bool $public
     */
    public function setPublic(bool $public): void
    {
        $this->public = $public;
    }

    /**
     * @return string
     */
    public function getSnapshotId(): string
    {
        return $this->snapshotId;
    }

    /**
     * @param string $snapshotId
     */
    public function setSnapshotId(string $snapshotId): void
    {
        $this->snapshotId = $snapshotId;
    }

    /**
     * @return PlaylistTrackList
     */
    public function getTracks(): PlaylistTrackList
    {
        return $this->tracks;
    }

    /**
     * @param PlaylistTrackList $tracks
     */
    public function setTracks(PlaylistTrackList $tracks): void
    {
        $this->tracks = $tracks;
    }

    /**
     * @return ObjectType
     */
    public function getType(): ObjectType
    {
        return $this->type;
    }

    /**
     * @param ObjectType $type
     */
    public function setType(ObjectType $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri(string $uri): void
    {
        $this->uri = $uri;
    }
}
