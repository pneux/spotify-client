<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Playlist;

use SpotifyClient\Model\ItemList;

class PlaylistList extends ItemList
{
    /** @var SimplifiedPlaylist[]  */
    private array $items;

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
