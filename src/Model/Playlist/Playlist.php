<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Playlist;

use SpotifyClient\Model\Followers\Followers;

class Playlist extends SimplifiedPlaylist
{
    private Followers $followers;

    /**
     * @return Followers
     */
    public function getFollowers(): Followers
    {
        return $this->followers;
    }

    /**
     * @param Followers $followers
     */
    public function setFollowers(Followers $followers): void
    {
        $this->followers = $followers;
    }
}
