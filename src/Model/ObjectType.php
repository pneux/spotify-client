<?php declare(strict_types=1);

namespace SpotifyClient\Model;

enum ObjectType: string
{
    case Album = 'album';
    case Artist = 'artist';
    case User = 'user';
    case Playlist = 'playlist';
    case Track = 'track';
}
