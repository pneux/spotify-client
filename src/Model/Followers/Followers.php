<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Followers;

class Followers
{
    private ?string $href;
    private int     $total;

    /**
     * @param string|null $href
     * @param int $total
     */
    public function __construct(?string $href, int $total)
    {
        $this->href  = $href;
        $this->total = $total;
    }

    /**
     * @return string|null
     */
    public function getHref(): ?string
    {
        return $this->href;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}
