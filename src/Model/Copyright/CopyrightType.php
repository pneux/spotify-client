<?php

namespace SpotifyClient\Model\Copyright;

enum CopyrightType: string
{
    case Copyright = 'C';
    case PerformaceCopyright = 'P';
}
