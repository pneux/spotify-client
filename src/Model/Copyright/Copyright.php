<?php

namespace SpotifyClient\Model\Copyright;

class Copyright
{
    private ?string        $text;
    private ?CopyrightType $type;
}
