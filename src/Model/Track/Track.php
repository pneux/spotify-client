<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Track;

use SpotifyClient\Model\Album\Album;
use SpotifyClient\Model\Artist\Artist;
use SpotifyClient\Model\ExternalIds;
use SpotifyClient\Model\ExternalUrls;
use SpotifyClient\Model\ObjectType;
use SpotifyClient\Model\Playlist\PlaylistTrackTrack;
use SpotifyClient\Model\Restrictions;
use SpotifyClient\Model\User\MyTopItem;

class Track implements PlaylistTrackTrack, MyTopItem
{
    private Album $album;

    /** @var Artist[] */
    private array $artists;

    /** @var string[] */
    private array $availableMarkets;

    private int           $discNumber;
    private int           $durationMs;
    private bool          $explicit;
    private ExternalIds   $externalIds;
    private ExternalUrls  $externalUrls;
    private string        $href;
    private string        $id;
    private bool          $isPlayable;
    private ?Restrictions $restriction;
    private string        $name;
    private int           $popularity;
    private ?string       $previewUrl;
    private int           $trackNumber;
    private ObjectType    $type;
    private string        $uri;
    private bool          $isLocal;

    /**
     * @return Album
     */
    public function getAlbum(): Album
    {
        return $this->album;
    }

    /**
     * @return array
     */
    public function getArtists(): array
    {
        return $this->artists;
    }

    /**
     * @return array
     */
    public function getAvailableMarkets(): array
    {
        return $this->availableMarkets;
    }

    /**
     * @return int
     */
    public function getDiscNumber(): int
    {
        return $this->discNumber;
    }

    /**
     * @return int
     */
    public function getDurationMs(): int
    {
        return $this->durationMs;
    }

    /**
     * @return bool
     */
    public function isExplicit(): bool
    {
        return $this->explicit;
    }

    /**
     * @return ExternalIds
     */
    public function getExternalIds(): ExternalIds
    {
        return $this->externalIds;
    }

    /**
     * @return ExternalUrls
     */
    public function getExternalUrls(): ExternalUrls
    {
        return $this->externalUrls;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isPlayable(): bool
    {
        return $this->isPlayable;
    }

    /**
     * @return Restrictions|null
     */
    public function getRestriction(): ?Restrictions
    {
        return $this->restriction;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPopularity(): int
    {
        return $this->popularity;
    }

    /**
     * @return string|null
     */
    public function getPreviewUrl(): ?string
    {
        return $this->previewUrl;
    }

    /**
     * @return int
     */
    public function getTrackNumber(): int
    {
        return $this->trackNumber;
    }

    /**
     * @return ObjectType
     */
    public function getType(): ObjectType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return bool
     */
    public function isLocal(): bool
    {
        return $this->isLocal;
    }
}
