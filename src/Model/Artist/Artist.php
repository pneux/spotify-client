<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Artist;

use SpotifyClient\Model\Followers\Followers;
use SpotifyClient\Model\Image;
use SpotifyClient\Model\User\MyTopItem;

class Artist extends SimplifiedArtist implements MyTopItem
{
    private Followers $followers;

    /** @var string[]|null */
    private array $genres;

    /** @var Image[]|null */
    private array $images;

    private ?int $popularity;

    /**
     * @return Followers
     */
    public function getFollowers(): Followers
    {
        return $this->followers;
    }

    /**
     * @return array
     */
    public function getGenres(): array
    {
        return $this->genres;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @return int|null
     */
    public function getPopularity(): ?int
    {
        return $this->popularity;
    }
}
