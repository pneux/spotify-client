<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Artist;

use SpotifyClient\Model\ExternalUrls;
use SpotifyClient\Model\ObjectType;

class SimplifiedArtist
{
    protected ExternalUrls $externalUrls;
    protected string       $href;
    protected string       $id;
    protected string       $name;
    protected ObjectType   $type;
    protected string       $uri;

    /**
     * @return ExternalUrls
     */
    public function getExternalUrls(): ExternalUrls
    {
        return $this->externalUrls;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ObjectType
     */
    public function getType(): ObjectType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }
}
