<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model;

class ExternalIds
{
    private ?string $isrc;
    private ?string $ean;
    private ?string $upc;

    /**
     * @return string|null
     */
    public function getIsrc(): ?string
    {
        return $this->isrc;
    }

    /**
     * @return string|null
     */
    public function getEan(): ?string
    {
        return $this->ean;
    }

    /**
     * @return string|null
     */
    public function getUpc(): ?string
    {
        return $this->upc;
    }
}
