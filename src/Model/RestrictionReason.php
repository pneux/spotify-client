<?php

namespace SpotifyClient\Model;

enum RestrictionReason: string
{
    case Market = 'market';
    case Product = 'product';
    case Explicit = 'explicit';
}
