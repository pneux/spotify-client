<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model;

class ExternalUrls
{
    private string $spotify;

    /**
     * @param string $spotify
     */
    public function __construct(string $spotify)
    {
        $this->spotify = $spotify;
    }

    /**
     * @return string
     */
    public function getSpotify(): string
    {
        return $this->spotify;
    }

    /**
     * @param string $spotify
     */
    public function setSpotify(string $spotify): void
    {
        $this->spotify = $spotify;
    }
}
