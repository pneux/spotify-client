<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\Album;

use SpotifyClient\Model\Artist\SimplifiedArtist;
use SpotifyClient\Model\Copyright\Copyright;
use SpotifyClient\Model\ExternalIds;
use SpotifyClient\Model\ExternalUrls;
use SpotifyClient\Model\Image;
use SpotifyClient\Model\ObjectType;
use SpotifyClient\Model\Restrictions;
use DateTimeInterface;

class Album
{
    private AlbumType $albumType;
    private int       $totalTracks;

    /** @var string[] */
    private array $availableMarkets;

    private ExternalUrls $externalUrls;

    private string $href;
    private string $id;

    /** @var Image[] */
    private array $images;

    private string                    $name;
    private DateTimeInterface         $releaseDate;
    private AlbumReleaseDatePrecision $releaseDatePrecision;
    private ?Restrictions             $restrictions;
    private ObjectType                $type;
    private string                    $uri;

    /** @var Copyright[]|null */
    private ?array $copyrights;

    private ?ExternalIds $externalIds;

    /** @var string[]|null */
    private ?array $genres;

    private ?string $label;
    private ?int    $popularity;

    private ?AlbumGroup $albumGroup;

    /** @var SimplifiedArtist[]|null */
    private ?array $artists;

    /**
     * @return AlbumType
     */
    public function getAlbumType(): AlbumType
    {
        return $this->albumType;
    }

    /**
     * @return int
     */
    public function getTotalTracks(): int
    {
        return $this->totalTracks;
    }

    /**
     * @return array
     */
    public function getAvailableMarkets(): array
    {
        return $this->availableMarkets;
    }

    /**
     * @return ExternalUrls
     */
    public function getExternalUrls(): ExternalUrls
    {
        return $this->externalUrls;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return DateTimeInterface
     */
    public function getReleaseDate(): DateTimeInterface
    {
        return $this->releaseDate;
    }

    /**
     * @return AlbumReleaseDatePrecision
     */
    public function getReleaseDatePrecision(): AlbumReleaseDatePrecision
    {
        return $this->releaseDatePrecision;
    }

    /**
     * @return Restrictions|null
     */
    public function getRestrictions(): ?Restrictions
    {
        return $this->restrictions;
    }

    /**
     * @return ObjectType
     */
    public function getType(): ObjectType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return array|null
     */
    public function getCopyrights(): ?array
    {
        return $this->copyrights;
    }

    /**
     * @return ExternalIds|null
     */
    public function getExternalIds(): ?ExternalIds
    {
        return $this->externalIds;
    }

    /**
     * @return array|null
     */
    public function getGenres(): ?array
    {
        return $this->genres;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return int|null
     */
    public function getPopularity(): ?int
    {
        return $this->popularity;
    }

    /**
     * @return AlbumGroup|null
     */
    public function getAlbumGroup(): ?AlbumGroup
    {
        return $this->albumGroup;
    }

    /**
     * @return array|null
     */
    public function getArtists(): ?array
    {
        return $this->artists;
    }
}
