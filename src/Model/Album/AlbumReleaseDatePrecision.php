<?php declare(strict_types=1);

namespace SpotifyClient\Model\Album;

enum AlbumReleaseDatePrecision: string
{
    case Year = 'year';
    case Month = 'month';
    case Day = 'day';
}
