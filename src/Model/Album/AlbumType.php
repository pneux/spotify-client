<?php declare(strict_types=1);

namespace SpotifyClient\Model\Album;

enum AlbumType: string
{
    case Album = 'album';
    case Single = 'single';
    case Compilation = 'compilation';
}
