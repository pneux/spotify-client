<?php declare(strict_types=1);

namespace SpotifyClient\Model\Album;

enum AlbumGroup: string
{
    case Album = 'album';
    case Single = 'single';
    case Compilation = 'compilation';
    case AppearsOn = 'appears_on';
}
