<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\User;

use SpotifyClient\Model\Image;

class User extends UserBase
{
    private ?string $displayName;

    /** @var Image[]  */
    private array $images;

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }
}
