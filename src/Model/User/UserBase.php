<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\User;

use SpotifyClient\Model\ExternalUrls;
use SpotifyClient\Model\Followers\Followers;
use SpotifyClient\Model\ObjectType;

class UserBase
{
    protected ExternalUrls $externalUrls;
    protected Followers    $followers;
    protected string       $href;
    protected string       $id;
    protected ObjectType   $type;
    protected string       $uri;

    /**
     * @return ExternalUrls
     */
    public function getExternalUrls(): ExternalUrls
    {
        return $this->externalUrls;
    }

    /**
     * @return Followers
     */
    public function getFollowers(): Followers
    {
        return $this->followers;
    }

    /**
     * @return string
     */
    public function getHref(): string
    {
        return $this->href;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return ObjectType
     */
    public function getType(): ObjectType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }
}
