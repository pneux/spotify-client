<?php

namespace SpotifyClient\Model\User;

use SpotifyClient\Model\ItemList;

class MyTopItemsList extends ItemList
{
    /** @var MyTopItem[]  */
    private array $items;

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }
}
