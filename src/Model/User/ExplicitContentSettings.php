<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\User;

class ExplicitContentSettings
{
    private bool $filterEnabled;
    private bool $filterLocked;

    /**
     * @return bool
     */
    public function isFilterEnabled(): bool
    {
        return $this->filterEnabled;
    }

    /**
     * @return bool
     */
    public function isFilterLocked(): bool
    {
        return $this->filterLocked;
    }
}