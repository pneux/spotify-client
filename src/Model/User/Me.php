<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Model\User;

class Me extends User
{
    private ?string $country;
    private ?string $email;
    private ?ExplicitContentSettings $explicitContent;
    private ?string $product;

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return ExplicitContentSettings|null
     */
    public function getExplicitContent(): ?ExplicitContentSettings
    {
        return $this->explicitContent;
    }

    /**
     * @return string|null
     */
    public function getProduct(): ?string
    {
        return $this->product;
    }
}