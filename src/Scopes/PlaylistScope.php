<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Scopes;

use AutoMapperPlus\Exception\UnregisteredMappingException;
use GuzzleHttp\Exception\GuzzleException;
use SpotifyClient\Model\Playlist\Playlist;
use SpotifyClient\Model\Playlist\PlaylistList;
use SpotifyClient\Model\Playlist\PlaylistTrack;
use SpotifyClient\Model\Playlist\PlaylistTrackList;
use SpotifyClient\Request\Api\Playlists\GetMyPlaylistsRequest;
use SpotifyClient\Request\Api\Playlists\GetPlaylistItemsRequest;
use SpotifyClient\Request\Api\Playlists\GetPlaylistRequest;

class PlaylistScope extends ClientScope
{
    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function getMyPlaylists(string $token): PlaylistList
    {
        $request = new GetMyPlaylistsRequest();
        $res     = $request->send($this->client, $token);
        return $this->mapper->map($res, PlaylistList::class);
    }

    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function getPlaylist(string $token, string $playlistId): Playlist
    {
        $request = new GetPlaylistRequest($playlistId);
        $res     = $request->send($this->client, $token);
        return $this->mapper->map($res, Playlist::class);
    }

    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function getPlaylistItems(string $token, string $playlistId, ?int $limit = null, ?int $offset = null): PlaylistTrackList
    {
        $request = new GetPlaylistItemsRequest($playlistId, $limit, $offset);
        $res     = $request->send($this->client, $token);
        return $this->mapper->map($res, PlaylistTrackList::class);
    }
}