<?php

namespace SpotifyClient\Scopes;

use GuzzleHttp\Client;
use SpotifyClient\Request\ResponseMapper;

abstract class ClientScope
{
    protected Client         $client;
    protected ResponseMapper $mapper;

    public function __construct(Client $client, ResponseMapper $mapper)
    {
        $this->client = $client;
        $this->mapper = $mapper;
    }
}