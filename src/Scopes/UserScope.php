<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Scopes;

use AutoMapperPlus\Exception\UnregisteredMappingException;
use GuzzleHttp\Exception\GuzzleException;
use SpotifyClient\Model\User\Me;
use SpotifyClient\Model\User\MyTopItemsList;
use SpotifyClient\Model\User\User;
use SpotifyClient\Request\Api\Users\CheckIfUsersFollowPlaylistRequest;
use SpotifyClient\Request\Api\Users\GetMeRequest;
use SpotifyClient\Request\Api\Users\GetMyTopItemsRequest;
use SpotifyClient\Request\Api\Users\GetUserRequest;
use SpotifyClient\Request\Api\Users\MyTopItemTimeRange;
use SpotifyClient\Request\Api\Users\MyTopItemType;

class UserScope extends ClientScope
{
    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function getMe(string $token): Me
    {
        $request = new GetMeRequest();
        $res     = $request->send($this->client, $token);
        return $this->mapper->map($res, Me::class);
    }

    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function getUser(string $token, string $userId): User
    {
        $request = new GetUserRequest($userId);
        $res     = $request->send($this->client, $token);
        return $this->mapper->map($res, User::class);
    }

    /**
     * @throws GuzzleException
     */
    public function checkIfUsersFollowPlaylist(string $token, string $playlistId, array $userIds): array
    {
        $request = new CheckIfUsersFollowPlaylistRequest($playlistId, $userIds);
        $res     = $request->send($this->client, $token);
        return array_combine($userIds, json_decode((string) $res->getBody(), true));
    }

    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function getMyTopItems(
        string              $token,
        MyTopItemType       $type,
        ?MyTopItemTimeRange $timeRange = null,
        ?int                $limit = null,
        ?int                $offset = null
    ): MyTopItemsList
    {
        $request = new GetMyTopItemsRequest($type, $timeRange, $limit, $offset);
        $res     = $request->send($this->client, $token);
        return $this->mapper->map($res, MyTopItemsList::class);
    }
}