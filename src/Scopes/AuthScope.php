<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace SpotifyClient\Scopes;

use AutoMapperPlus\Exception\UnregisteredMappingException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use SpotifyClient\Model\Auth\TokenResponse;
use SpotifyClient\Request\Api\Auth\RefreshTokenRequest;
use SpotifyClient\Request\Api\Auth\RetrieveTokenRequest;
use SpotifyClient\Request\ResponseMapper;
use SpotifyClient\SpotifyClientConfiguration;

class AuthScope extends ClientScope
{
    private SpotifyClientConfiguration $configuration;

    public function __construct(Client $client, ResponseMapper $mapper, SpotifyClientConfiguration $configuration)
    {
        parent::__construct($client, $mapper);
        $this->configuration = $configuration;
    }

    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function retrieveToken(string $code, string $redirectUri): TokenResponse
    {
        $request = new RetrieveTokenRequest($code, $redirectUri);
        $token   = base64_encode(sprintf('%s:%s', $this->configuration->getClientId(), $this->configuration->getClientSecret()));
        $res     = $request->send($this->client, $token);
        return $this->mapper->map($res, TokenResponse::class);
    }

    /**
     * @throws UnregisteredMappingException
     * @throws GuzzleException
     */
    public function refreshToken(string $refreshToken): TokenResponse
    {
        $request       = new RefreshTokenRequest($refreshToken);
        $token         = base64_encode(sprintf('%s:%s', $this->configuration->getClientId(), $this->configuration->getClientSecret()));
        $res           = $request->send($this->client, $token);
        $tokenResponse = $this->mapper->map($res, TokenResponse::class);
        if (!array_key_exists('refresh_token', json_decode((string)$res->getBody(), true))) {
            $tokenResponse->setRefreshToken($refreshToken);
        }
        return $tokenResponse;
    }
}